import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Var, Op, OutVar, Data, Scalar, BlockInfo} from './types';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {
  }

  private baseUrl = 'http://localhost:12344';
  private scalarUrl = '/scalar';

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  // tslint:disable-next-line:typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getData(url: string): Observable<BlockInfo[]> {
    return this.http.get<BlockInfo[]>(url)
      .pipe(catchError(this.handleError<BlockInfo[]>('get graph data', {} as BlockInfo[])));
  }

  getScalar(): Observable<Scalar[]> {
    // TODO should modify interface to adjust to multiple scalar charts
    const url = this.baseUrl + this.scalarUrl;
    return this.http.get<{ data: Scalar[] }>(url)
      .pipe(map(x => x.data))
      .pipe(catchError(this.handleError<Scalar[]>('get scalar data', [] as Scalar[])));
  }

}
