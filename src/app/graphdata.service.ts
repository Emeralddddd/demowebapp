import {Injectable} from '@angular/core';
import {DataService} from './data.service';
import {BlockGraph, BlockInfo, Data, Graph} from './types';
import {Observable, of} from 'rxjs';
import {last, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GraphdataService {

  private baseUrl = 'http://localhost:12344';
  private graphUrl = '/graph';
  private newNodeCounter = 600;

  constructor(private dataService: DataService) {
  }

  retrieveHop(): Observable<BlockInfo[]> {
    return this.dataService.getData(this.baseUrl + this.graphUrl + '/hop');
  }

  retrieveRuntime(): Observable<BlockInfo[]> {
    return this.dataService.getData(this.baseUrl + this.graphUrl + '/runtime');
  }

  getBlockList(data: BlockInfo[]): BlockInfo[] {
    const res: BlockInfo[] = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i].type === 'generic') {
        res.push(data[i]);
      } else if (data[i].sub) {
        res.push(...this.getBlockList(data[i].sub));
      } else if (data[i].else_sub) {
        res.push(...this.getBlockList(data[i].else_sub));
      }
    }
    return res;
  }

  genHop(data: Data): Graph {
    const graph: Graph = {nodes: [], edges: []};
    if (data.vars != null) {
      data.vars.forEach(x => {
        const style = 'stroke: #333;fill:#fff;stroke-width: 2px;';
        graph.nodes.push({
          id: Number(x.index),
          label: x.symbol,
          dataShape: x.shape,
          size: x.size,
          node: x.node,
          type: 'var',
          opType: '',
          style
        });
      });
    }
    data.ops.forEach(x => {
      const style = 'stroke: #333;fill:#d3d3d3;stroke-width: 2px;';
      graph.nodes.push({
        id: Number(x.index),
        label: `${x.type}(${x.symbol})`,
        dataShape: x.shape,
        size: x.size,
        node: x.node,
        type: 'op',
        opType: x.type,
        style
      });
      x.source.split(',').forEach(src => {
        graph.edges.push({source: Number(src), target: Number(x.index), label: ''});
      });
    });

    if (data.outvars != null) {
      data.outvars.forEach(x => {
        const style = 'stroke: #333;fill:#fff;stroke-width: 2px;';
        graph.nodes.push({
          id: Number(x.index),
          label: x.symbol,
          dataShape: x.shape,
          size: x.size,
          node: x.node,
          type: 'outvar',
          opType: '',
          style
        });
        x.source.split(',').forEach(src => {
          graph.edges.push({source: Number(src), target: Number(x.index), label: ''});
        });
      });
    }
    return graph;
  }

  // TODO fix arrow to iteration variables
  genRuntime(data: Data): Graph {
    const graph: Graph = {nodes: [], edges: []};
    const symbol2index = new Map();
    const varsSet = new Set();
    data.vars.forEach(x => {
      symbol2index.set(x.label, Number(x.index));
      if (!varsSet.has(x.index)) {
        const style = 'stroke: #333;fill:#fff;stroke-width: 2px;';
        graph.nodes.push({
          id: Number(x.index),
          label: x.symbol,
          dataShape: x.shape,
          size: '',
          node: x.node,
          type: 'var',
          opType: '',
          style
        });
        varsSet.add(x.index);
      }
    });

    const addNode = (src: string, style: string) => {
      graph.nodes.push({
        id: this.newNodeCounter,
        label: src,
        dataShape: '',
        size: '',
        node: '',
        type: 'var',
        opType: '',
        style
      });
      symbol2index.set(src, this.newNodeCounter);
      this.newNodeCounter++;
    };
    if (data.outvars != null) {
      data.outvars.forEach(x => {
        symbol2index.set(x.symbol, Number(x.index));
        const style = 'stroke: #333;fill:#fff;stroke-width: 2px;';
        graph.nodes.push({
          id: Number(x.index),
          label: x.symbol,
          dataShape: x.shape,
          size: x.size,
          node: x.node,
          type: 'outvar',
          opType: '',
          style
        });
        x.source.split(',').forEach(src => {
          if (!symbol2index.has(src)) {
            addNode(src, style);
          }
          graph.edges.push({source: symbol2index.get(src), target: Number(x.index), label: ''});
        });
      });
    }
    data.ops.forEach(x => {
      // symbol2index[x.target] = x.index;
      const styleVar = 'stroke: #333;fill:#fff;stroke-width: 2px;';
      const styleOp = 'stroke: #333;fill:#d3d3d3;stroke-width: 2px;';
      // graph.nodes.push({
      //   id: Number(x.index),
      //   label: x.symbol,
      //   dataShape: '',
      //   size: '',
      //   node: x.node,
      //   type: 'op',
      //   opType: '',
      //   style
      // });
      graph.nodes.find(t => t.id === symbol2index.get(x.target)).label = x.symbol;
      x.source.split(' ').forEach(src => {
        if (!symbol2index.has(src)) {
          addNode(src, styleVar);
        }
        graph.edges.push({source: symbol2index.get(src), target: symbol2index.get(x.target), label: ''});
        const target = graph.nodes.find(item => item.id === symbol2index.get(x.target));
        target.type = 'op';
        target.style = styleOp;
      });
    });
    const nodeSet = new Set();
    graph.edges.forEach(x => {
      nodeSet.add(x.target);
      nodeSet.add(x.source);
    });
    graph.nodes = graph.nodes.filter(x => nodeSet.has(x.id));
    return graph;
  }

  genBlock(blockInfos: BlockInfo[]): BlockGraph {
    const graph: BlockGraph = {nodes: [], edges: []};
    this.genOneBlock(blockInfos, graph);
    return graph;
  }

  genOneBlock(blockInfos: BlockInfo[], graph: BlockGraph): number[] {
    let res: number[] = [];
    for (let i = 0; i < blockInfos.length; i++) {
      const nowBlock = blockInfos[i];
      const lastNodes: number[] = [];
      if (nowBlock.type === 'generic') {
        graph.nodes.push({id: Number(nowBlock.block_id), label: 'generic'});
        if (i < blockInfos.length - 1) {
          graph.edges.push({source: Number(nowBlock.block_id), target: Number(blockInfos[i + 1].block_id)});
        } else {
          res = [Number(blockInfos.slice(-1)[0].block_id)];
        }
      } else if (nowBlock.type === 'while') {
        graph.nodes.push({id: Number(nowBlock.block_id), label: 'while'});
        graph.edges.push({source: Number(nowBlock.block_id), target: Number(nowBlock.sub[0].block_id)});
        lastNodes.push(...this.genOneBlock(nowBlock.sub, graph));
        if (i < blockInfos.length - 1) {
          lastNodes.forEach(x => {
            graph.edges.push({source: x, target: Number(blockInfos[i + 1].block_id)});
          });
        } else {
          res = lastNodes;
        }
        graph.edges.push({source: Number(nowBlock.sub.slice(-1)[0].block_id), target: Number(nowBlock.block_id)});
      } else if (nowBlock.type === 'if') {
        graph.nodes.push({id: Number(nowBlock.block_id), label: 'if'});
        if (nowBlock.sub != null) {
          graph.edges.push({source: Number(nowBlock.block_id), target: Number(nowBlock.sub[0].block_id)});
          lastNodes.push(...this.genOneBlock(nowBlock.sub, graph));
        }
        if (nowBlock.else_sub != null) {
          graph.edges.push({source: Number(nowBlock.block_id), target: Number(nowBlock.else_sub[0].block_id)});
          lastNodes.push(...this.genOneBlock(nowBlock.else_sub, graph));
        }
        if (i < blockInfos.length - 1) {
          lastNodes.forEach(x => {
            graph.edges.push({source: x, target: Number(blockInfos[i + 1].block_id)});
          });
        } else {
          res = lastNodes;
        }
      }
    }
    return res;
  }

  getBlock(): Observable<BlockGraph> {
    return this.dataService.getData(this.baseUrl + this.graphUrl + '/hop').pipe(map(x => this.genBlock(x)));
  }
}
