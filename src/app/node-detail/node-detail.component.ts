import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {NodeInfo} from '../types';

@Component({
  selector: 'app-node-detail',
  templateUrl: './node-detail.component.html',
  styleUrls: ['./node-detail.component.css']
})
export class NodeDetailComponent implements OnInit {
  @Input() selectedNode: { nodeInfo: NodeInfo, inputInfo: NodeInfo[], outputInfo: NodeInfo[] };
  panelOpenState = true;
  @Output() clickEvent = new EventEmitter<string>();

  constructor() {
  }

  handleClick(v: string): void {
    console.log(v);
    this.clickEvent.emit(v);
  }

  ngOnInit(): void {
  }

}
