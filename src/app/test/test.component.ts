import {Component, OnInit} from '@angular/core';
import {GraphdataService} from '../graphdata.service';
import * as dagreD3 from 'dagre-d3';
import {BlockGraph} from '../types';
import * as d3 from 'd3';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor(private graphdataService: GraphdataService) {
  }

  private dataset: BlockGraph;
  private g: dagreD3.graphlib.Graph<{}>;

  getBlockData(): void {
    this.graphdataService.getBlock().subscribe(x => {
      this.dataset = x;
      this.draw();
    });
  }

  draw(): void {
    this.g = new dagreD3.graphlib.Graph();
    this.g.setGraph({rankdir: 'BT', nodesep: 20});
    this.dataset.nodes.forEach(item => {
      this.g.setNode(String(item.id), {
        style: item.label === 'generic' ? 'stroke: #333;fill:#fff;stroke-width: 2px;' : 'stroke: #333;fill:#d3d3d3;stroke-width: 2px',
        // 节点标签
        label: item.label + ' ' + String(item.id),
        // 节点形状
        labelStyle: 'font-size:20px',
        labelType: 'html', width: 100, height: 36,
      });
    });
    this.dataset.edges.forEach(item => {
      this.g.setEdge(String(item.source), String(item.target), {
        style: 'fill:#fff;stroke:#333;stroke-width:2.5px',
        curve: d3.curveBasis,
      });
    });
    this.g.nodes().forEach(v => {
      const node = this.g.node(v);
      node.rx = 10;
      node.ry = 10;
    });
    const render = new dagreD3.render();
    const svg = d3.select('#graph');
    svg.selectAll('*').remove();
    const inner = svg.append('g');

    const zoom = d3.zoom()
      .on('zoom', () => {
        inner.attr('transform', d3.event.transform);
      });
    svg.call(zoom);
    // @ts-ignore
    render(inner, this.g);
    const main = document.getElementById('main');
    const scaleRate = (main.clientHeight / this.g.graph().height) * 0.8;
    svg.call(zoom.transform, d3.zoomIdentity.scale(scaleRate).translate(10, 10));
  }

  ngOnInit(): void {
    this.getBlockData();
  }

}
