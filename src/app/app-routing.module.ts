import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GraphComponent} from './graph/graph.component';
import {ScalarComponent} from './scalar/scalar.component';
import {TestComponent} from './test/test.component';

const routes: Routes = [
  // {path: '', redirectTo: '/graph', pathMatch: 'full'},
  {path: 'graph', component: GraphComponent},
  {path: 'scalar', component: ScalarComponent},
  {path: 'test', component: TestComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
