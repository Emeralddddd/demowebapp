import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {MatSliderModule} from '@angular/material/slider';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderComponent} from './header/header.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {AppRoutingModule} from './app-routing.module';
import {MatButtonModule} from '@angular/material/button';
import {GraphComponent} from './graph/graph.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatExpansionModule} from '@angular/material/expansion';
import { NodeDetailComponent } from './node-detail/node-detail.component';
import { ScalarComponent } from './scalar/scalar.component';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import { TestComponent } from './test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GraphComponent,
    NodeDetailComponent,
    ScalarComponent,
    TestComponent
  ],
    imports: [
        BrowserModule,
        MatSliderModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatIconModule,
        AppRoutingModule,
        MatButtonModule,
        MatTabsModule,
        HttpClientModule,
        MatSidenavModule,
        MatExpansionModule,
        MatSelectModule,
        MatInputModule,
        FormsModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
