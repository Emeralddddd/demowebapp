export interface Var {
  index: string;
  symbol: string;
  shape: string;
  size: string;
  node: string;
  label?: string;
}

export interface Op {
  index: string;
  type: string;
  symbol: string;
  source: string;
  shape?: string;
  size?: string;
  node: string;
  target?: string;
}

export interface OutVar {
  index: string;
  symbol: string;
  source: string;
  shape: string;
  size: string;
  node: string;
}

export interface Data {
  vars: Var[];
  ops: Op[];
  outvars: OutVar[];
}

export interface Scalar {
  iter: number;
  value: number;
}


export interface Graph {
  nodes: { id: number, label: string, dataShape: string, size: string, node: string, type: string, opType: string, style: string }[];
  edges: { source: number, target: number, label: string }[];
}

export interface NodeInfo {
  id: string;
  label: string;
  type: string;
  shape: string;
  size: string;
  loc: string;
  opType: string;
}

export interface BlockInfo {
  block_id: string;
  type: string;
  sub: BlockInfo[];
  else_sub: BlockInfo[];
  data: Data;
}

export interface GraphInfo {
  block_id: string;
  graph: Graph;
}

export interface BlockGraph {
  nodes: { id: number, label: string }[];
  edges: { source: number, target: number }[];
}
