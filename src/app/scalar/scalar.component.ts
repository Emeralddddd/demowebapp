import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import * as d3 from 'd3';
import {Scalar} from '../types';

@Component({
  selector: 'app-scalar',
  templateUrl: './scalar.component.html',
  styleUrls: ['./scalar.component.css']
})
export class ScalarComponent implements OnInit {
  private data: Scalar[];
  private tooltip;
  private filePath = '/Users/zhizhenxu/dev/systemml_test/logs/statistics.log';
  private margin = ({top: 20, right: 130, bottom: 30, left: 40});
  private height = 300;
  private width = 600;
  private svg;
  private focus;
  private mousemove;

  constructor(private dataService: DataService) {
  }

  draw(): void {
    const innerWidth = this.width - this.margin.left - this.margin.right;
    const innerHeight = this.height - this.margin.top - this.margin.bottom;


    this.svg = d3.select('#chartdiv').append('svg')
      .attr('viewBox', [0, 0, this.width, this.height] as any);
    // .attr('height', height)
    // .attr('width', width);

    // const line = d3.line<Scalar>()
    //   .defined(d => !isNaN(d.value))
    //   .x(d => x(d.iter))
    //   .y(d => y(d.value))
    //   // .curve(d3.curveCatmullRom.alpha(0.5));
    //   .curve(d3.curveCardinal.tension(0.5));

    this.svg.append('g')
      .attr('class', 'Xaxis');

    this.svg.append('g')
      .attr('class', 'Yaxis');

    // this.svg.append('path')
    //   .datum(this.data)
    //   .attr('fill', 'none')
    //   .attr('stroke', 'steelblue')
    //   .attr('stroke-width', 1.5)
    //   .attr('stroke-linejoin', 'round')
    //   .attr('stroke-linecap', 'round')
    //   .attr('d', line);

    this.svg.transition().delay(1000).duration(1000);

    const focus = this.svg.append('g')
      .attr('class', 'focus')
      .style('display', 'none');

    focus.append('circle')
      .attr('r', 4)
      .attr('fill', 'steelblue');

    focus.append('rect')
      .attr('class', 'tooltip')
      .attr('fill', 'white')
      .attr('stroke', '#000')
      .attr('width', 100)
      .attr('height', 50)
      .attr('x', 10)
      .attr('y', -22)
      .attr('rx', 4)
      .attr('ry', 4);

    focus.append('text')
      .attr('class', 'tooltip-iter')
      .attr('font-weight', 'bold')
      .attr('font-size', '14px')
      .attr('x', 18)
      .attr('y', -2);

    focus.append('text')
      .attr('x', 18)
      .attr('y', 18)
      .text('Loss:');

    focus.append('text')
      .attr('class', 'tooltip-loss')
      .attr('x', 60)
      .attr('y', 18);

    this.focus = focus;
// tslint:disable-next-line:typedef
    this.svg.append('rect')
      .attr('class', 'overlay')
      .attr('width', innerWidth)
      .attr('height', innerHeight)
      .attr('transform', `translate(${this.margin.left},${this.margin.top})`);
  }

  update(): void {
    const x = d3.scaleLinear()
      .domain(d3.extent(this.data, d => d.iter))
      .range([this.margin.left, this.width - this.margin.right])
      .nice();

    const y = d3.scaleLinear()
      .domain(d3.extent(this.data, d => d.value))
      .range([this.height - this.margin.bottom, this.margin.top])
      .nice();

    const xAxis = g => g
      .attr('transform', `translate(0,${this.height - this.margin.bottom})`)
      .attr('color', '#d3d3d3')
      .call(d3.axisBottom(x).ticks(innerWidth / 100).tickSizeOuter(0).tickSize(-innerHeight));

    const yAxis = g => g
      .attr('transform', `translate(${this.margin.left},0)`)
      .attr('color', '#d3d3d3')
      .call(d3.axisLeft(y).ticks(innerHeight / 100).tickSizeOuter(0).tickSize(-innerWidth));

    this.svg.selectAll('.Xaxis').transition().duration(500).call(xAxis);
    this.svg.selectAll('.Yaxis').transition().duration(500).call(yAxis);

    const line1 = d3.line<Scalar>()
      .defined(d => !isNaN(d.value))
      .x(d => x(d.iter))
      .y(d => y(d.value))
      .curve(d3.curveCardinal.tension(0.5));

    const line = this.svg.selectAll('.lineTest ').data([this.data]);

    line.enter().append('path').attr('class', 'lineTest').merge(line).transition().duration(500)
      .attr('d', line1)
      .attr('fill', 'none')
      .attr('stroke', 'steelblue')
      .attr('stroke-width', 1.5)
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round');

    const focus = this.focus;
    const margin = this.margin;
    const data = this.data;

    const bisectDate = d3.bisector((d: Scalar) => {
      return d.iter;
    }).left;

    // tslint:disable-next-line:typedef
    function mousemove() {
      const x0 = x.invert(d3.mouse(this)[0] + margin.left);
      const i = bisectDate(data, x0, 1);
      const d0 = data[i - 1];
      const d1 = data[i];
      const d = x0 - d0.iter > d1.iter - x0 ? d1 : d0;
      focus.attr('transform', 'translate(' + x(d.iter) + ',' + y(d.value) + ')');
      focus.select('.tooltip-iter').text(`Iter ${d.iter}`);
      focus.select('.tooltip-loss').text(d.value.toFixed(4));
    }

    this.svg.selectAll('.overlay').on('mouseover', () => {
      focus.style('display', null);
    })
      .on('mouseout', () => {
        focus.style('display', 'none');
      })
      .on('mousemove', mousemove);
    this.focus = focus;
  }


  ngOnInit(): void {
    this.tooltip = d3.select('.tooltip');
    this.draw();
    // this.dataService.getScalar().subscribe(x => {
    //   this.data = x;
    //   this.draw();
    // });
    this.getScalarTest();
  }

  getScalarTest(): void {
    const times: Scalar[] = [];
    const sleep = (ms) => {
      return new Promise(resolve => setTimeout(resolve, ms));
    };
    const loop = async () => {
      for (let i = 0; i < 10; i++) {
        console.log(this.data);
        times.push({iter: i, value: Math.random() * 10});
        await sleep(2000);
        this.data = times;
        this.update();
      }
    };
    loop();
  }
}
