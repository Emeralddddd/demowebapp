import {Component, OnInit} from '@angular/core';
import {GraphdataService} from '../graphdata.service';
import * as dagreD3 from 'dagre-d3';
import * as d3 from 'd3';
import {NodeInfo, Graph, BlockGraph, BlockInfo, Data} from '../types';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  constructor(private graphdataService: GraphdataService) {
  }

  private g: dagreD3.graphlib.Graph<{}>;
  private tooltip;
  private nodeInfos: Map<string, NodeInfo> = new Map();
  private dataset: Graph;
  private blcokDataset: BlockGraph;
  public selectedNode: { nodeInfo: NodeInfo, inputInfo: NodeInfo[], outputInfo: NodeInfo[] };
  private preSelectedIndex = 0;
  public selectedExplain: string;
  private hopData: BlockInfo[] = null;
  private hopList: BlockInfo[] = null;
  private runtimeData: BlockInfo[] = null;
  private runtimeList: BlockInfo[] = null;


  changeExplain(): void {
    if (this.selectedExplain === 'hop') {
      this.getHopData();
    } else if (this.selectedExplain === 'runtime') {
      // this.getRuntimeData();
    }
  }

  selectNode(v): void {
    const info = this.nodeInfos.get(v);
    const input = this.g.inEdges(v).map(x => this.nodeInfos.get(x.v));
    const output = this.g.outEdges(v).map(x => this.nodeInfos.get(x.w));
    this.selectedNode = {nodeInfo: info, inputInfo: input, outputInfo: output};
  }

  getHopData(): void {
    if (!this.hopData) {
      this.graphdataService.retrieveHop().subscribe(x => {
        this.hopData = x;
        this.hopList = this.graphdataService.getBlockList(this.hopData);
      });
    }
  }

  async getRuntimeData(): Promise<void> {
    if (!this.runtimeData) {
      this.runtimeData = await this.graphdataService.retrieveRuntime().toPromise();
      this.runtimeList = this.graphdataService.getBlockList(this.runtimeData);
    }
  }

  tipVisible(v): void {
    this.tooltip.transition()
      .duration(400)
      .style('opacity', 0.9)
      .style('display', 'block');
    const info = this.nodeInfos.get(v);
    this.tooltip.html(`DataShape:${info.shape}\nSize:${info.size}\nNode:${info.loc}`)
      .style('left', (d3.event.pageX + 15) + 'px')
      .style('top', (d3.event.pageY + 15) + 'px');
  }

  tipHidden(): void {
    this.tooltip.transition()
      .duration(400)
      .style('opacity', 0)
      .style('display', 'none');
  }

  draw(): void {
    this.g = new dagreD3.graphlib.Graph();
    // this.tooltip = this.createTooltip();
    this.tooltip = d3.select('.tooltip');
    this.g.setGraph({rankdir: 'BT', nodesep: 36});
    this.dataset.nodes.forEach(item => {
      this.nodeInfos.set(String(item.id),
        {
          id: String(item.id),
          label: item.label,
          shape: item.dataShape,
          size: item.size,
          loc: item.node,
          type: item.type,
          opType: item.opType
        });

      this.g.setNode(String(item.id), {
        // 节点标签
        label: item.label,
        // 节点形状
        shape: item.type === 'op' ? 'rect' : 'ellipse',
        // 节点样式
        style: item.style,
        labelStyle: 'font-size:20px',
        labelType: 'html', width: 100, height: 36,
      });
    });
    this.dataset.edges.forEach(item => {
      this.g.setEdge(String(item.source), String(item.target), {
        // 边标签
        label: item.label,
        // 边样式
        style: 'fill:#fff;stroke:#333;stroke-width:2.5px',
        curve: d3.curveBasis,
      });
    });
    this.g.nodes().forEach(v => {
      const node = this.g.node(v);
      node.rx = 10;
      node.ry = 10;
    });
    const render = new dagreD3.render();
    const svg = d3.select('#graph');
    svg.selectAll('*').remove();
    const inner = svg.append('g');

    const zoom = d3.zoom()
      .on('zoom', () => {
        inner.attr('transform', d3.event.transform);
      });
    svg.call(zoom);
    // @ts-ignore
    render(inner, this.g);
    const main = document.getElementById('main');
    const scaleRate = (main.clientHeight / this.g.graph().height) * 0.8;
    svg.call(zoom.transform, d3.zoomIdentity.scale(scaleRate).translate(10, 10));

    svg.on('click', () => {
      this.selectedNode = null;
      document.querySelector('g.nodes').children[this.preSelectedIndex].firstElementChild.classList.remove('selected');
    });
    inner.selectAll('g.node')
      .on('mouseover', (v) => {
        this.tipVisible(v);
      }).on('mouseout', () => this.tipHidden())
      .on('click', (v, i) => {
        this.addSelectedClass(i);
        this.selectNode(v);
        d3.event.stopPropagation();
      });
  }

  drawBlock(mode: string): void {
    let data: BlockGraph = null;
    if (mode === 'hop') {
      data = this.graphdataService.genBlock(this.hopData);
    } else if (mode === 'runtime') {
      data = this.graphdataService.genBlock(this.runtimeData);
    } else {
      return;
    }
    this.g = new dagreD3.graphlib.Graph();
    this.g.setGraph({rankdir: 'BT', nodesep: 20});
    data.nodes.forEach(item => {
      this.g.setNode(String(item.id), {
        style: item.label === 'generic' ? 'stroke: #333;fill:#fff;stroke-width: 2px;' : 'stroke: #333;fill:#d3d3d3;stroke-width: 2px',
        // 节点标签
        label: item.label + ' ' + String(item.id),
        // 节点形状
        labelStyle: 'font-size:20px',
        labelType: 'html', width: 100, height: 36,
      });
    });
    data.edges.forEach(item => {
      this.g.setEdge(String(item.source), String(item.target), {
        style: 'fill:#fff;stroke:#333;stroke-width:2.5px',
        curve: d3.curveBasis,
      });
    });
    this.g.nodes().forEach(v => {
      const node = this.g.node(v);
      node.rx = 10;
      node.ry = 10;
    });
    const render = new dagreD3.render();
    const svg = d3.select('#graph');
    svg.selectAll('*').remove();
    const inner = svg.append('g');

    const zoom = d3.zoom()
      .on('zoom', () => {
        inner.attr('transform', d3.event.transform);
      });
    svg.call(zoom);
    // @ts-ignore
    render(inner, this.g);
    const main = document.getElementById('main');
    const scaleRate = (main.clientHeight / this.g.graph().height) * 0.8;
    svg.call(zoom.transform, d3.zoomIdentity.scale(scaleRate).translate(10, 10));
    inner.selectAll('g.node')
      .on('click', (v: string, i) => {
        this.swtichToBlock(v);
        d3.event.stopPropagation();
      });
  }

  private addSelectedClass(i: number): void {
    const nodes = document.querySelector('g.nodes');
    nodes.children[this.preSelectedIndex].firstElementChild.classList.remove('selected');
    nodes.children[i].firstElementChild.classList.add('selected');
    this.preSelectedIndex = i;
  }

  clickNode(v: string): void {
    const i = this.g.nodes().indexOf(v);
    this.addSelectedClass(i);
    this.selectNode(v);
  }

  swtichToBlock(id: string): void {
    let list: BlockInfo[];
    if (this.selectedExplain === 'hop') {
      list = this.hopList;
      const block = list.find(x => x.block_id === id);
      if (block) {
        this.dataset = this.graphdataService.genHop(block.data);
      }
    } else if (this.selectedExplain === 'runtime') {
      list = this.runtimeList;
      const block = list.find(x => x.block_id === id);
      if (block) {
        this.dataset = this.graphdataService.genRuntime(block.data);
      }
    } else {
      return;
    }
    this.draw();
  }

  async ngOnInit(): Promise<void> {
    this.getRuntimeData();
    await this.getRuntimeData();
    console.log(this.runtimeList);
    this.drawBlock(this.selectedExplain);
  }
}
