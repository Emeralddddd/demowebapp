import * as fs from 'fs';

const watch = require('watch');
// const data = fs.readFileSync('./runtime.log');
// const text = data.toString();
// const blocks = data.toString().split(/-*GENERIC.*[\s\S]/);
// const lines = blocks[3].toString().split('\n');
// const opList: Op[] = [];
// const pattern = RegExp(/-*(CP|SP) (\S*)( \S+\.(MATRIX|SCALAR)\.\S*)+/);
// const varpattern = RegExp(/ (\S+)\.(MATRIX|SCALAR)\.\S*/g);
// let counter = 0;
// for (const line of lines) {
//   const tmp1 = pattern.exec(line);
//   if (tmp1 === null) {
//     continue;
//   }
//   const node = tmp1[1];
//   const name = tmp1[2];
//   let vars = varpattern.exec(line);
//   // while (vars) {
//   //   opList.push({});
//   // }
//   // opList.push()
//   console.log(line);
// }
//
// export interface Op {
//   name: string;
//   source: string;
//   target: string;
// }

// console.log(JSON.parse('{"iter":0,"time":1.03147}'));

const filePath = '/Users/zhizhenxu/dev/systemml_test/logs/statistics.log';
fs.watch(filePath, (event, filename) => {
  console.log(fs.existsSync(filePath));
  if (fs.existsSync(filePath)) {
    const lines = fs.readFileSync(filePath).toString().trim().split('\n');
    const data = lines.map(x => JSON.parse(x));
    console.log(data);
  }
});
